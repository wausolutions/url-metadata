module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
        {
      name: 'misc-url-metadata',
      script: 'server.js',
      error_file: 'error.log',
      out_file: 'out.log',
      pid_file: 'misc.pid',
      log_date_format: "YYYY-MM-DD HH:mm Z",
      max_memory_restart: '500M',
      exec_mode: "cluster",
      instances: 1,
      env: {
        PORT: 3020
      }
    }
  ],
};
