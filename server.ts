// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
import * as bodyParser from "body-parser";
import Debug from "debug";
import * as dotenv from "dotenv";
import * as express from "express";
// import cors from "./src/application/configs/cors";
import UrlToMetadata from "./src/application/Helpers/UrlToMetadata";

const debug: any = Debug("meta");
const app = express();                 // define our app using express

dotenv.config();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.use(cors());

const port = process.env.PORT || 3020;        // set our port

// ROUTES FOR OUR API
// =============================================================================
const router = express.Router();              // get an instance of the express Router

router.get("/meta", UrlToMetadata.fixUrl, (req: express.Request, res: express.Response, next: any) => {
  (new UrlToMetadata()).resolve(req.body.url, (error, response) => {
    if (error) {
      return next(error);
    } else {
      res.json({
        meta: {
          description: !!response.description ? response.description : "(No description found)",
          displayLink: !!response.displayLink ? response.displayLink : "",
          image: !!response.image ? response.image : "no.jpg",
          title: !!response.description ? response.description : "(No title found)",
          url: response.url,
        },
        result: { status: "OK" },
      });
    }
  });
});

// more routes for our API will happen here

// allow cors
app.use((req, res, next) => {

  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  res.setHeader("Access-Control-Allow-Headers",
    "Origin, X-Requested-With, X-User-Id, X-Portal-Id, X-Requested-By, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use("/", router);

// START THE SERVER
// =============================================================================
app.listen(port);
debug("woking on " + port);

