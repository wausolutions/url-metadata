import * as cors from "cors";

/**
 * Configures the CORS : https://medium.com/@alexishevia/using-cors-in-express-cac7e29b005b
 *
 * @returns {*}
 */
const init = (): any => {

  // credentials: true for holding cookie
  return cors({
    // origin:"http://localhost:3000",
    credentials: true,
    origin: (origin, callback) => {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) {
        return callback(null, true);
      }
// tslint:disable-next-line: no-shadowed-variable
      const allowedOrigins = process.env.CORS_ALLOWED_ORIGINS.split(",").map((origin: any) => {
        return origin.trim();
      });
      if (allowedOrigins.indexOf(origin) === -1) {
        const msg = "The CORS policy for this site does not " +
          "allow access from the specified Origin.";
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    }
  });

};

export default init;
