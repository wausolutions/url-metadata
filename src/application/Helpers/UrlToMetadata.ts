import { parallel } from "async";
import { NextFunction, Request, Response } from "express";
import { get } from "request";
import * as urlmetadata from "url-metadata";
import { resolve } from "./Metascraper";

// tslint:disable-next-line: interface-over-type-literal
export type Meta = {
  url?: string,
  description?: string,
  title?: string,
  image?: string,
  displayLink?: string
};

export default class UrlToMetadata {

  public static fixUrl(req: Request, res: Response, next: NextFunction) {
    req.body.url = req.query.url ? req.query.url : "";
    if (!!req.body.url) {
      const httpCheck = "http://";
      if (!req.body.url.match(/^http(s)?:\/\//)) {
        req.body.url = httpCheck + req.body.url;
      }
    }
    return next();
  }

  public resolve(url: string, callback: (error: Error, response: Meta) => void) {
    parallel([
      (afterGoogleMetadata: (error: Error, response: any) => void) => {
        this.googleMetadata(url, afterGoogleMetadata);
      },
      (afterUrlMetadata: (error: Error, response: any) => void) => {
        this.urlMetadata(url, afterUrlMetadata);
      },
      (afterMetascraper: (error: Error, response: any) => void) => {
        this.metascraperMetadata(url, afterMetascraper);
      },
    ], (error: Error, response: any) => {
      // const metadata: any = {};
      return callback(error, this.mergeMetadata(response));
    });
  }

  protected mergeMetadata(metadata: Meta[]): Meta {
    let meta: Meta;
    meta = {
      description: "",
      displayLink: "",
      image: "",
      title: "",
      url: ""
    };

    metadata.forEach((item: Meta) => {
      if (!!item.description) {
        meta.description = item.description;
      }
      if (!!item.displayLink) {
        meta.displayLink = item.displayLink;
      }
      if (!!item.image) {
        meta.image = item.image;
      }
      if (!!item.title) {
        meta.title = item.title;
      }
      if (!!item.url) {
        meta.url = item.url;
      }
    });

    return meta;
  }

  /**
   * Resolve link using url-metadata
   * @param url url
   * @param callback callback function
   */
  private urlMetadata(url: string, callback: (error: Error, meta: Meta) => void) {
    urlmetadata(url).then((metadata: any) => {
      callback(null, {
        description: metadata.description,
        image: metadata.image,
        title: metadata.title,
        url: metadata.url
      });
    }).catch((error: Error) => callback(error, null));
  }

  private googleMetadata(url: string, callback: (error: Error, meta: Meta) => void) {
    // tslint:disable-next-line: max-line-length
    get(`https://www.googleapis.com/customsearch/v1?key=${process.env.GOOGLE_URL_API_KEY}&cx=010294765002421679506:g_jcwh7r_pk&q="${url}"`, (error: any, response: any, body: any) => {
      let b: any;
      if (response.statusCode === 200) {
        try {
          b = JSON.parse(body);
        } catch (error) {
          b = {};
        }
        if (b.items && b.items.length > 0) {
          return callback(error, {
            displayLink: b.items.length > 0 ? b.items[0].displayLink : "",
            title: b.items.length > 0 ? b.items[0].title : "",
            url: b.items.length > 0 ? b.items[0].link : "",
          });
        } else {
          return callback(null, {});
        }
      } else {
        return callback(new Error("Error when searching from gooel"), null);
      }
    });
  }

  private metascraperMetadata(url: string, callback: (error: Error, meta: Meta) => void) {
    resolve(url, (error, response) => {

      return callback(error, {
        description: response.description,
        image: response.image,
        title: response.title,
        url: response.url,
      });

    });
  }

}
