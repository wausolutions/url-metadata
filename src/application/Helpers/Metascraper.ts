
import * as metascraper from "metascraper";
import * as author from "metascraper-author";
import * as clearbit_logo from "metascraper-clearbit-logo";
import * as date from "metascraper-date";
import * as description from "metascraper-description";
import * as image from "metascraper-image";
import * as logo from "metascraper-logo";
import * as publisher from "metascraper-publisher";
import * as title from "metascraper-title";
import * as url from "metascraper-url";
// import { get } from "request";

const scraper = metascraper([
  author(),
  date(),
  description(),
  image(),
  logo(),
  clearbit_logo(),
  publisher(),
  title(),
  url()
]);

// tslint:disable-next-line: no-var-requires
const got = require("got");

export const resolve = (targetUrl: string, callback: (error: Error, response: any) => void) => {
  // get(targetUrl, (error: Error, response: any, body: any) => {
  //   if (error) {
  //     return callback(error, null);
  //   }
  //   console.log(response, body);
  //   if (response.status === 200) {
  //     return callback(null, JSON.parse(body));
  //   } else {
  //     return callback(new Error('Error when fetching data'), null);
  //   }
  // });
  (async () => {
// tslint:disable-next-line: no-shadowed-variable
    const { body: html, url } = await got(targetUrl);
    const data = await scraper({ html, url });
    callback(null, data);
  })();
};
